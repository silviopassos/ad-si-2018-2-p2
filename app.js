var net = require("net");
var app = require('express')();
var server = require('http').Server(app);
var io = require('socket.io').listen(server);
var fs = require('fs');
var path = require('path');	

server.listen(8080, function() {
    console.log('Listening at: http://localhost:8080');
});

app.get('*', function (req, res) {
            var url = req.originalUrl.match(/\/(.*)/);
            //console.log(url);            
            if ((!url) || (!url[1])) {
                res.sendFile(path.join(__dirname, 'index.html'));
            }
            else {
                res.sendFile(path.join(__dirname, url[1]));
            }
        });


// Trata conexão com os clientes (Proxy)
io.sockets.on('connection', function (webSocket) {
	
	var tS = new net.Stream;
	tS.setTimeout(0);
	tS.setEncoding("ascii"); //UTF8 é padrão
	
	webSocket.on('message', function (message) {
		try
		{
			//se comando = CONNECT 
			if(message.split(' ')[0] == 'CONNECT')
			{
				//Conecta ao servidor IRC fornecido via TCP
				var host = message.split(' ')[1].split(':')[0];
				var port = message.split(' ')[1].split(':')[1];
				console.log( 'connecting to '+host+':'+port+'…' );
				tS.destroy();
				tS.connect( port, host );
			}
			else
			{
				//Encaminha mensagem para o servidor remoto via TCP
				tS.write(message);
			}
		}
		catch (e)
		{
			webSocket.send(e);
		}
	});	

	//Fecha a conexão com o servidor IRC quando a conexão com o cliente é fechada
	webSocket.on('disconnect', function () {
		tS.end();
	});

	//Encaminha mensagens de erro para o cliente
	tS.addListener("error", function (error){
		//Encaminha mensagem de erro para o websocket
		webSocket.send(error+"\r\n");
	});
	
	//Trata os dados recebidos do servidor IRC
	tS.addListener("data", function (data) {
		//Encaminha os dados para o websocket
		webSocket.send(data);
	});	
	
	//Notifica o cliente quando a conexão com o servidor é fechada.
	tS.addListener("close", function (){
		webSocket.send("Conexão fechada pelo servidor. Use o comando /CONNECT para conectar.");
	});
});