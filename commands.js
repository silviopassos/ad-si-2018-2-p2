// mudar para canal inserido pelo usuário no login
var current_channel = 'server';

// tratamento mensagem
function parse_msg(text) {
	var data = text.split(" ", 4);
	if (data.length == 4 && data[1] == 'PRIVMSG') {
		var user = text.split('!~')[0].replace(':', '');
		var message = text.replace(/^:[^:]*:/, '').replace(/[\n\r]/g, '');
		var receiver = null;
		
		/* Testa se canal ou usuário */
		if (data[2].charAt(0) == '#') {
			receiver = data[2]; // canal
		} else {
			receiver = user; // user
		}
		return { 
				'valid'    : 1, 
				'user'     : user, 
				'channel'  : data[2], 
				'receiver' : receiver, 
				'message'  : message
			}
	} else {
		// mensagem válida
		return { 'valid' : 0 }
	}
}

var socket = io.connect(window.location.host);

$(document).ready(function () {
	$("#ui").hide();
	$('#connect').css('padding-top', 
		(($(window).innerHeight() - $('#connect').outerHeight()) / 2) + 'px');

	$("#connect-btn").click(function(e) {
		e.preventDefault();
		/* Tratar intarface tosca… */
		$("#connect").hide(); //hide
		$("#ui").show();
		$('#top-box').height($(window).innerHeight() - $('#bottom-box').outerHeight() - 25);
		$('#top-box ul').height($('#top-box').innerHeight() - 2);

		// Connectar ao server IRC
		socket.send("CONNECT "+ $("#server").val());
		socket.send("NICK "+ $("#nick").val() +"\n");
		socket.send("USER "+ $("#nick").val() + ' '
			+ $("#server").val().split(":")[0] 
			+ " blubb :"+$("#nick").val() +"\n");


		//Adicionar manipulador ao formulário de entrada
		$('#bottom-box form').submit(function() {
			var msg = $('#msginput').val();
			$('#msginput').val('');
			
			// Checar comandos

			// join
			if (msg.match(/^\/join .+/i)) {
				socket.send('JOIN ' + msg.split(' ')[1] + "\r\n");

			// nick
			} else 	if (msg.match(/^\/nick .+/i)) {
				if (msg.split(' ')[1].match(/^([a-zA-Z0-9]|[-[]\\`\^{}])+$/))
				{
					socket.send('NICK ' + msg.split(' ')[1] + "\r\n"); 
					$("#nick").val(msg.split(' ')[1]); //salvar nick para conectar
				}
				else
				{
					showMsg("Erro: Nickname inválido", current_channel);
				}
				
			// help
			} else 	if (msg.match(/^\/help.*/i)) {
				showHelp();

			// connect
			} else 	if (msg.match(/^\/connect .+/i)) {
				socket.send("CONNECT "+ msg.split(' ')[1]);
				socket.send("NICK "+ $("#nick").val() +"\n");
				socket.send("USER "+ $("#nick").val() + ' '
					+ $("#server").val().split(":")[0] 
					+ " blubb :"+$("#nick").val() +"\n");

			// part
			} else 	if (msg.match(/^\/part(.+)?$/i)) {
				if(msg.split(' ')[1])  	//canal informado
					socket.send('PART ' + msg.split(' ')[1] + "\r\n"); 
				else			// nenhum canal informado
					socket.send('PART ' + current_channel + "\r\n"); 

			// query
			} else 	if (msg.match(/^\/query .+/i)) {
				var username = msg.split(' ')[1];
				if ($('#'+username).length == 0)
				{
					switch_channel(username);
					$('#channel').append('<li onclick="switch_channel($(this).text());" id="' 
						+ username + '">' + username + '</li>');
					showMsg('Conversa privada com '+username, current_channel);
				}

			// msg
			} else 	if (msg.match(/^\/msg .+ .+/i)) {
				var username = msg.split(' ')[1];
				var message = msg.substr(msg.substr(5).indexOf(' ')+6);
				socket.send("PRIVMSG "+username+" :"+message+"\r\n");
				showMsg($("#nick").val()+ " -> "+ username +" : " + message, "server");
				showMsg($("#nick").val()+ " : " + message, username);

			// comando desconhecido	
			} else if (msg.match(/^\/.*/i)) {
				showMsg("Error: comando desconhecido ou faltam argumentos. Digite /help para ajuda.", current_channel);

			} else {
				/* No command. Send as message to current channel */
				socket.send("PRIVMSG "+current_channel+" :"+msg+"\r\n");
				showMsg($("#nick").val()+" : " + msg, current_channel);
			}
			return false;
		});
	});

});


function showMsg(msg, channel){
	msg = $('<div/>').text(msg).html();  // escape msg
	$("#messages").append('<li '
			+ ((channel != current_channel) ? 'style="display:none;"' : '') +
			' class="' + channel.replace(/^#/, '') + '">' + msg + "</li>").ready(function(){ 
		var scrollbox = document.getElementById('messages');
		scrollbox.scrollTop = scrollbox.scrollHeight;
	});
}


function switch_channel(c) {

	$('#messages li').css('display', 'none');
	$('li.' + c.replace(/^#/, '')).css('display', 'block');
	current_channel = c;

}


function showHelp(){
	showMsg("--------------------------------------", current_channel);
	showMsg("Comandos disponíveis:", current_channel);
	showMsg("/join <channel>		    : Junta-se ao canal", current_channel);
	showMsg("/nick <nickname>		    : Altera o nickname", current_channel);
	showMsg("/part [<channel>]		    : Deixa o canal informado ou o atual se nenhum canal é informado", current_channel);
	showMsg("/connect <hostname>:<port>	: Conecta-se ao servidor fornecido", current_channel);
	showMsg("/query <user>			    : Abre uma conversa privada com um usuário", current_channel);
	showMsg("/msg <user> <message>		: Envia uma mensagem para o usuário especificado sem abrir um novo bate-papo.", current_channel);
	showMsg("--------------------------------------"	, current_channel);
}


socket.on('message', function (data) {
	var lines = data.split("\r\n");
	for (var i = 0; i < lines.length; i++)  {
		// play ping pong
		if (lines[i].match(/^PING.*/)) {
			socket.send('PONG ' + lines[i].split(' ')[1] + '\r\n');
		} else if (lines[i] != '' ) {
			var msg = parse_msg(lines[i]);
			if (msg.valid) {
				// Verifica se o canal existe. Se não -> adicione-o como um novo (query)
				if ($('#'+msg.receiver.replace(/^#/, '')).length == 0)
					$('#channel').append('<li onclick="switch_channel($(this).text());" id="' 
						+ msg.receiver + '">' + msg.receiver + '</li>');
				showMsg(msg.user + ' : ' + msg.message, msg.receiver);
			} else {
				msg = lines[i].split(' ');
				if (msg.length > 2 && msg[1] == 'JOIN') {
					if(msg[0].slice(1, msg[0].indexOf('!'))== $('#nick').val())
					{
						// própria mensagem de JOIN -> canal aberto
						switch_channel(msg[2]);
						$('#channel').append('<li onclick="switch_channel($(this).text());" id="' 
							+ msg[2].replace(/^#/, '') + '">' + msg[2] + '</li>');
					}
					else
					{
						// JOIN de outro usuário -> atualizar lista de usuários
					}

				} else if (msg.length > 2 && msg[1] == 'PART') {
					if(msg[0].slice(1, msg[0].indexOf('!')) == $('#nick').val())
					{
						// própria mensagem PART -> sair do canal
						switch_channel('server');
						$(msg[2]).remove();
					}
					else
					{
						// mensagem PART de outro usuário -> atualizar as listas de usuários dos canais afetados
					}
				// Modo de canal
				} else if (msg.length > 4 && msg[1] == '332') { 
					showMsg(lines[i].replace(/^:[^:]*:/, ''), msg[3]);

				// Usuários no canal	
				} else if (msg.length > 4 && msg[1] == '353') {
					showMsg('Usuários no canal: ' + lines[i].split(':')[2], msg[4]);
				
				// Modo de usuário alterado
				} else if (msg.length > 2 && msg[1] == 'MODE') {
					showMsg(lines[i].replace(/^.* MODE /, 'Modo '), msg[2]);

				} else {
					showMsg(lines[i], 'server');
				}
			}
		}
	}
});